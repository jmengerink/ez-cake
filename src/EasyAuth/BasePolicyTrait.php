<?php


namespace EZCake\EasyAuth;


use Authorization\IdentityInterface;
use Authorization\Policy\RequestPolicyInterface;
use Cake\Core\App;
use Cake\Http\Exception\MissingControllerException;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;

trait BasePolicyTrait {

	/**
	 * Will invoke the isAuthorized method with the correct parameters extracted from the current request
	 * @param IdentityInterface|null $identity
	 * @param ServerRequestInterface $request
	 * @return bool
	 * @throws Exception if a non-cake request is passed
	 */
	private function delegateAuthToControler(?IdentityInterface $identity, ServerRequest $request): bool {

		$plugin = $request->getParam('plugin');
		$controller = $request->getParam('controller');
		$action = $request->getParam('action');
		$pass = $request->getParam('pass');
		$user = $this->unpack($identity);


		$class = $controller;
		if (!empty($plugin)) {
			$class = $plugin . '.' . $class;
		}

		$policyClassName = App::className($class, 'Policy', 'Policy');
		if ($policyClassName === null) {
			throw new MissingControllerException("Trying to get {$policyClassName} but no such policy found");
		}

		/** @var RequestPolicyInterface $policy */
		$policy = new $policyClassName();
		return $policy->isAuthorized($user, $request, $action, $pass);

		throw new RuntimeException('Non-cake request passed');
	}

	/**
	 * Unpacks an identity object into an optional {@link UserInterface}
	 * @param mixed $identity Either a {@link \Authorization\IdentityInterface}, {@link \Authentication\IdentityInterface}, {@link User}, or null
	 * @return mixed the identity object
	 * @noinspection MultipleReturnStatementsInspection
	 */
	private function unpack($identity) {
		if ($identity === null) {
			return null;
		}

		/**
		 * Leave this for distinguishability between Authentication and Authorization identities
		 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
		 */
		if ($identity instanceof \Authorization\IdentityInterface) {
			$identity = $identity->getOriginalData();
		}

		if ($identity === null) {
			return null;
		}

		if ($identity instanceof \Authentication\IdentityInterface) {
			$identity = $identity->getOriginalData();
		}

		return $identity;
	}

}
