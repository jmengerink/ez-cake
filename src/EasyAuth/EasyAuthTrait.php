<?php
/**
 * Created by PhpStorm.
 * User: lordphnx
 * Date: 12/12/17
 * Time: 8:26 PM
 */

namespace EZCake\EasyAuth;

use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;
use Closure;
use Exception;
use EZCake\EasyAuth\error\MissingParameterException;
use RuntimeException;


/**
 * The easyAuthTrait helps in creating easy
 * Should be included on your AppController
 *
 * @property ServerRequest request
 *
 * @package UserSystem
 */
trait EasyAuthTrait {

	/**
	 * IDArgumentAuth assumes that most Authorization actions depend on entities, that have to be fetched based on id's in parameters
	 *
	 * e.g.
	 * /posts/edit/1, which edits post #1, is only allowed if the current user is the owner of that post.
	 * For this we have to fetch Post #1 from the database, and do a check against it.
	 *
	 * $this->IDArgAuth() takes the repetitiveness out of this task.
	 * By telling it which argument index belongs to which ModelAlias,
	 *
	 * It will fetch them for you, and pass them to a callable you provide, which should return a boolean to determine
	 *
	 * $this->IDArgAuth([0=>"Posts"], function(Post $p) use ($user) {
	 *  return $p->user_id == $user->user_id
	 * }
	 *
	 * @param ServerRequest $request The serverRequest to authorize
	 * @param array $modelTypeMap
	 * @param $callable
	 *
	 * @return bool
	 * @throws RuntimeException if the primary key passed is not valid
	 */
	protected function IDArgAuth(ServerRequest $request, array $modelTypeMap, Closure $callable) {
		$objects = [];
		foreach ($modelTypeMap as $index => $type) {
			$table = TableRegistry::getTableLocator()->get($type);
			try {
				$params = $request->getParam("pass");
				if (!isset($params[$index])) {
					throw new MissingParameterException("Not all required pass parameters were present (#" . $index . ")");
				}
				$id = $params[$index];
				$objects[] = $table->get($id);
			} catch (InvalidPrimaryKeyException $ex) {
				throw new RuntimeException("Identifier #{$index} ({$id}) {$type} not found");
			}
		}

		if (count($objects) === 1) {
			return $callable($objects[0]);
		}

		return $callable(...$objects);
	}

	/**
	 * When a GET request is made, takes query parameters and
	 *
	 * names of arguments passed into optional do not throw errors when not found
	 *
	 * @param ServerRequest $request
	 * @param array $modelTypeMap
	 * @param array $optional
	 * @param Closure $callable
	 *
	 * @throws Exception in case something is wrong
	 * @return bool
	 */
	protected function queryArgauth(ServerRequest $request, array $modelTypeMap, array $optional, Closure $callable) {
		$objects = [];
		foreach ($modelTypeMap as $name => $entityClass) {
			$table = TableRegistry::getTableLocator()->get($entityClass);
			try {

				$id = $request->getQuery($name);
				$objects[] = $table->get($id);
			} catch (RecordNotFoundException|InvalidPrimaryKeyException $ex) {
				if ($optional[$name]) {
					$objects[] = null;
				} else {
					throw $ex;
				}
			}
		}
		return $callable(...$objects);
	}

	/**
	 * When a POST request is performed, it takes the mapping from argument names in {@code modelTypeMap}, and fetches
	 * the Entity of that ModelType with the corresponding primary key
	 *
	 *
	 * @param ServerRequest $request The serverRequest to authorize
	 * @param array $modelTypeMap
	 * @param Closure $callable
	 *
	 * @return bool|mixed
	 * @throws Exception
	 */
	protected function PostArgAuth(ServerRequest $request, array $modelTypeMap, Closure $callable) {
		$objects = [];
		foreach ($modelTypeMap as $index => $type) {
			$table = TableRegistry::getTableLocator()->get($type);
			try {
				$id = $request->getData($index);
				$objects[] = $table->get($id);
			} catch (InvalidPrimaryKeyException $ex) {
				throw new Exception("Identifier #{$index} ({$id}) {$type} not found");
			}
		}

		return $callable(...$objects);
	}

}