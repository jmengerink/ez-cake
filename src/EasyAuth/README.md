Create an `AppPolicy.php` in `src/Policy/AppPolicy`

```php
class AppPolicy implements \Authorization\Policy\RequestPolicyInterface {

    use \EZCake\EasyAuth\BasePolicyTrait;

    public function canAccess(?\Authorization\IdentityInterface $identity,\Cake\Http\ServerRequest $request){
        return $this->delegateAuthToControler($identity, $request);
    }

    /**
     * @noinspection PhpUnusedParameterInspection
     * Non-use of any parameters is intentioanl, as the base policy is DECLINE
     *
     * @param User|null $user The {@link UserInterface} that is requesting access, or null if no user is authenticated
     * @param ServerRequest $request The {@link ServerRequestInterface} through which the {@code $user} is requesting access to the  {@code action}
     * @param string $action The action the user has requested
     * @param array $pass the arguments passed to the action
     * @return bool true iff this user is allowed to perform this action, false otherwise
     */
    public function isAuthorized(?User $user, ServerRequest $request, string $action, array $pass): bool {
        return false;
    }


}

```

Set up your AuthorizationService

```php
//in Application.php

class Application implements AuthenticationServiceProviderInterface, AuthorizationServiceProviderInterface {

    public function middleware($middlewareQueue): MiddlewareQueue {
        //...
        $middlewareQueue->add(new AuthenticationMiddleware($this));
        $middlewareQueue->add(new AuthorizationMiddleware($this, [
               
            'unauthorizedHandler' => [
                'className' => \EZCake\EasyAuth\MultiRedirectHandler::class,
                'queryParam' => 'redirect',
                'exceptions' => [
                    \Authorization\Exception\MissingIdentityException::class => '/users/login',
                    \Authorization\Exception\ForbiddenException::class => '/pages/forbidden'
                ],
            ],
        ]));
        $middlewareQueue->add(new RequestAuthorizationMiddleware());
    }

    //How do you want to authenticate?
    public function getAuthenticationService(ServerRequestInterface $request): AuthenticationServiceInterface {
        $service = new AuthenticationService();
        $service->loadAuthenticator('Authentication.Session');
        return $service;    
    }

    public function getAuthorizationService(ServerRequestInterface $request): AuthorizationServiceInterface {
        $resolver = new MapResolver();
        $resolver->map(ServerRequest::class, AppPolicy::class);
        return new AuthorizationService($resolver);
    }

}
```

Recommend you `User extends Entity implements IdentityInterface`

Now make policies. E.g. for your `ArticlesController.php` create `src/Policy/ArticlesPolicy.php`

```php
<?php
class ArticlesPolicy extends AppPolicy
{
    public function isAuthorized(?IdentityInterface $user, ServerRequest $request, string $action, array $pass): bool {
        switch ($action) {
            case 'get':
                //Recommend you use EasyAuthTrait :)            
                return $this->IDArgAuth($pass, [0 => 'Articles'], static function (?Article $art) use ($user) {
                    if ($art === null) {
                        return false;
                    }
                    return $article->isAuthor($user);
                });
            default:
                return parent::isAuthorized($user, $request, $action, $pass);
        } //.switch
    }
}

```