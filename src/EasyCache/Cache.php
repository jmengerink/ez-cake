<?php

namespace EZCake\EasyCache;

/**
 * @template K
 * @template V
 */
class Cache {
	protected $objects;

	public function __construct() {
		$this->objects = [];
	}

	/**
	 * @param K $key
	 * @param V $value
	 */
	public function cache($key, $value) {
		$this->objects[$key] = $value;
	}

	/**
	 * @psalm-param K $key
	 * @psalm-return V|null
	 */
	public function get($key) {
		if (isset($this->objects[$key])) {
			return $this->objects[$key];	
		}
		return null;
	}
}