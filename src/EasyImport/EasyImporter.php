<?php

namespace EZCake\EasyImport;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;

class EasyImporter {

	/**
	 * @param EntityInterface $entity
	 * @param array $updateFields
	 *
	 * @deprecated not yet implemented, use ormImport instead
	 */
	public static function import(EntityInterface $entity, array $updateFields) {
		//@todo: RatinggViewer-based UPDATE-fUELDS based indx-based imprort method
	}

    /**
     * ### Save options
     * The options array accepts the following keys:
     *
     * - atomic: Whether to execute the save and callbacks inside a database
     *   transaction (default: true)
     * - checkRules: Whether to check the rules on entity before saving, if the checking
     *   fails, it will abort the save operation. (default:true)
     * - associated: If `true` it will save 1st level associated entities as they are found
     *   in the passed `$entity` whenever the property defined for the association
     *   is marked as dirty. If an array, it will be interpreted as the list of associations
     *   to be saved. It is possible to provide different options for saving on associated
     *   table objects using this key by making the custom options the array value.
     *   If `false` no associated records will be saved. (default: `true`)
     * - checkExisting: Whether to check if the entity already exists, assuming that the
     *   entity is marked as not new, and the primary key has been set.
     *
     * @param EntityInterface $entity
     * @param Table $table
     * @param array $identifiers
     * @param array $saveOptions
     *
     * @return EntityInterface
     */
    public static function ormImport(EntityInterface $entity, Table $table, array $identifiers, array $saveOptions = []): EntityInterface {
		$conditions = self::buildConditions($identifiers, $entity);

		$existing = $table->find()
			->where($conditions)
			->first();

		if ($existing === null) {
			$existing = $table->newEmptyEntity();
		}

		$existing = $table->patchEntity($existing, $entity->toArray());
		$table->save($existing, $saveOptions);
		return $existing;
	}

	public static function buildConditions(array $identifiers, EntityInterface $entity) {
		$conditions = [];
		foreach ($identifiers as $fields) {

			if (!is_array($fields)) {
				$fields = [$fields];
			}

			$subCondition = [];

			foreach ($fields as $field) {
				$subCondition[$field . ' IS'] = $entity->get($field);
				$subCondition[$field . ' IS NOT'] = null;
			}

			$conditions[] = $subCondition;
		}

		return ['OR' => $conditions];
	}

}