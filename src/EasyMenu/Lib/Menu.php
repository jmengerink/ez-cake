<?php

namespace EZCake\EasyMenu\Lib;

class Menu {

	private $map;
	
	public function __construct() {
		$this->map = [];
	}

	/**
	 * @param Menu|MenuItem $item
	 */
	public function addItem($item) {
		$this-> map[] = $item;
	}

	/**
	 * @return Menu|MenuItem|null;
	 */
	public function get() {
		
	}

	/**
	 * @return MenuItem[]|Menu[]
	 */
	public function getAll() : array {
		return $this->map;
	}
}