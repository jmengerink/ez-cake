<?php

namespace EZCake\EasyMenu\Lib;

class MenuRegistry {

	private static $instance = null;

	private $map;

	public function __construct() {
		$this->map = [];
	}


	public function get($alias): ?Menu {
		return $this->map[$alias] ?? new Menu();
	}

	public function safeGet($alias): Menu {
		if (!isset($this->map[$alias])) {
			$this->map[$alias] = new Menu();
		}
		return $this->map[$alias];
	}

	public static function getInstance(): MenuRegistry {
		if (self::$instance === null) {
			self::$instance = new MenuRegistry();
		}
		return self::$instance;
	}


}