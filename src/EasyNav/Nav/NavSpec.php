<?php
declare(strict_types=1);


namespace OneBoard\Nav;


/**
 * Specification used in the generation of breadcrumbs/navs
 * @package OneBoard\Nav
 */
class NavSpec {

	/**
	 * @var string
	 */
	private $title;
	/**
	 * @var array|null
	 */
	private $parentUrl;

	/**
	 * @param string $title The title of this breadcrumb
	 * @param array|null $parentUrl The url of this breadcrumbs parent, or null if there is no parent
	 */
	public function __construct(string $title, ?array $parentUrl) {
		$this->title = $title;
		$this->parentUrl = $parentUrl;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @return array|null
	 */
	public function getParentUrl(): ?array {
		return $this->parentUrl;
	}

}
