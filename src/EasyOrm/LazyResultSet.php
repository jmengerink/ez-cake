<?php
declare(strict_types=1);

namespace EZCake\EasyOrm;;

use Cake\ORM\Query;
use Illuminate\Support\LazyCollection;

/**
 * Lazily iterates a query, to emulate {@link Query::disableBufferedResults()} without actually disabeling buffered results
 */
class LazyResultSet {

	/**
	 * @param Query $query
	 *
	 * @return LazyCollection
	 */
	public static function build(Query $query) {
		return LazyCollection::make(static function () use ($query) {
			$hasMore = true;
			$page = 1;
			$n = 100;

			do {
				$copy = $query->cleanCopy();
				$subResultSet = $copy->page($page, $n)->all();

				if ($subResultSet->isEmpty() || $subResultSet->count() < $n) {
					$hasMore = false;
				}

				foreach ($subResultSet as $item) {
					yield $item;
				}

				$page++;
			} while ($hasMore);

		});
	}

}