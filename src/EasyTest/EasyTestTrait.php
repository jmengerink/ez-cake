<?php
/**
 * Created by PhpStorm.
 * User: J.G.M. Mengerink
 * Date: 12/13/17
 * Time: 6:25 PM
 */

namespace EZCake\EasyTest;


use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use DateTime;
use Exception;
use EZCake\EasyTest\Error\EntityMockException;
use JsonException;
use OutOfBoundsException;
use RuntimeException;

/**
 * Trait EasyTestTrait
 *
 * The EasyTestTrait allows you to quickly create ORC mocking in your own Generic test classes
 * Using $this->genericCreate, you can minimally specify your stuff :)
 *
 * @package FenixDigitalSolutions\JoshUtils\EasyTest
 */
trait EasyTestTrait {
	
	/**
	 * @var int "Random" data
	 */
	private int $counter = 0;

	/**
	 * Randomly chooses an option from among the array elements
	 *
	 * @param array $options A list of things we can select from
	 *
	 * @return mixed an entry from the options array
	 */
	public function random(array $options): mixed {
		if (empty($options)) {
			throw new OutOfBoundsException("You provided an empty list of options to random()");
		}
		//@implNote, since !empty($options), it must be at least 1
		$n = count($options);
		try {
			$i = random_int(0, $n - 1);
			return array_values($options)[$i];
		} catch (Exception $e) {
			return array_values($options)[0];
		}

	}


	/**
	 * Creates an entity specified by the Model Class
	 * Any attribute not filled out, will be generated randomly based on the column type
	 *
	 *
	 * Example
	 * $this->genericCreate("User", ["email" => "j.mengerink@fenix-digital.nl"], ["username" => "lordphnx"], true)
	 * will create a User entity
	 * User(
	 *
	 * @param string $class The name of the CakePHP Model you want to use to create an entity
	 * @param array $values Set of values to use for the creation of the new Entity
	 * @param array $overrides
	 * @param bool $save if this parameter is set to true (default) the created entity will be persisted to database
	 * @param array $associated Recursive array of associated entities to use in NewEntity() creation
	 *
	 * @return EntityInterface the entity that is created (and possibly persisted)
	 * @throws EntityMockException in case we could not create your entity in the ORM layer
	 * @throws JsonException in case certain things cannot be \JsonException-serialized
	 */
	public function genericCreate(string $class, array $values, array $overrides = [], bool $save = true, array $associated = []): EntityInterface {
		$table = TableRegistry::getTableLocator()->get($class);
		$columns = $table->getSchema()->columns();

		/**
		 * Fill everything that is not already defined
		 */
		foreach ($columns as $columName) {

			//if an override value is present for the column, use that
			if (array_key_exists($columName, $overrides)) {
				$values[$columName] = $overrides[$columName];
			} //otherwise, generate non-PK values that are not present
			else {
				$type = $table->getSchema()->getColumnType($columName);
				$column = $table->getSchema()->getColumn($columName);

				$length = $column['length'] ?? null;

				//skip generating the primary key randomly
				if ($columName === $table->getPrimaryKey()) {
					continue;
				}


				if (!array_key_exists($columName, $values)) {
					$values[$columName] = $this->genericRandom($type, $length);
				}


			}

		}

		$entity = $table->newEntity($values, [
			'associated' => $associated,
			'guard' => false,
			'accessibleFields' => ['*' => true]
		]);


		if ($save && $table->save($entity, ['associated' => $associated]) === false) {
			throw new EntityMockException("Save of {$class} failed: " . print_r($entity->getErrors(), true));
		}
		return $entity;
	}


	/**
	 * Creates a random value of the specified class.
	 * Supports: String, text, int, integer, datetime, date, bool, boolean, float, double
	 *
	 * @param $class
	 * @param null $length
	 *
	 * @return bool|DateTime|int|string
	 * @throws JsonException
	 */
	public function genericRandom($class, $length = null): float|DateTime|bool|int|string {
		switch (strtolower($class)) {
			case "text":
			case "string":
				return $this->randomString($length ?? 32);
			case "int":
			case "integer":
				try {
					//because of lb=0 and the default of length, we can safely swallow this exception
					//@implNote, we choose 1E11 because of MySQL default integer constraints
					return $this->randomInt($length ?? 10 ^ 11, 0);
				} catch (Exception $exception) {
					return min(133742, $length);
				}
			case "datetime":
			case "date":
				return $this->randomDateTime();
			case "bool":
			case "boolean":
				return $this->randomBoolean();
			case "time":
				return $this->randomTime();


			case "json":
				return json_encode([
					"a" => $this->randomString(),
					"b" => $this->randomInt()
				], JSON_THROW_ON_ERROR);
			case "float":
			case "double":

				$first = 11;
				$second = 2;
				if ($length !== null) {
					$parts = explode(',', $length);
					$first = $parts[0];
					if (isset($parts[1])) {
						$second = $parts[1];
					}
				}
				
				return (float)"{$this->randomInt($first)}.{$this->randomInt($second)}";
			default:
				echo "No generic random yet for:" . $class . PHP_EOL;
				throw new RuntimeException("No generic random yet for:" . $class . PHP_EOL);
		}
	}

	/**
	 * Generates a random string
	 *
	 * @return string A random string value
	 */
	public function uniqueString(): string {
		$this->counter++;
		return "s" . $this->counter;
	}

	private static $alphabet = "abcdefghijklmnoipqrstuvwxyz";

	/**
	 * Generates a random string
	 *
	 * @param int $length the maximum length of the string
	 *
	 * @return string A random string value
	 */
	public function randomString(int $length = 10): string {
		if ($length === null) {
			$length = 15;
		}
		if ($length > 128) {
			$length = 128;
		}

		$length = $this->randomInt($length, 1);
		$str = "";
		for ($i = 0; $i < $length; $i++) {
			$str .= self::$alphabet[$this->randomInt(25, 0)];
		}
		return $str;
	}

	/**
	 * Gets a random integer
	 *
	 * @param int|null $ub The upper bound (inclusive)
	 * @param int|null $lb The lower bound  (inclusive)
	 *
	 * @return int
	 * @todo @josh hou rekening met
	 */
	public function randomInt(?int $ub = null, ?int $lb = null): int {
		if ($ub === null && $lb === null) {
			$this->counter++;
			return $this->counter;
		}

		try {
			return random_int($lb ?? 0, $ub ?? PHP_INT_MAX);
		} catch (Exception $exception) {
			return $this->random([$lb, $ub]);
		}

	}

	/**
	 * @return int a unique integer
	 */
	public function uniqueInt(): int {
		return $this->counter++;
	}

	/**
	 * @return boolean Returns true or false at random
	 */
	public function randomBoolean(): bool {
		return $this->random([true, false]);
	}

	/**
	 * Creates a random email address
	 *
	 * @return string
	 */
	public function randomEmail(): string {
		return $this->randomString() . "." . $this->randomString() . "@" . $this->randomString() . "." . $this->random([
				"com", "nl", "net", "de", "voice", "event"
			]);
	}

	public function uniqueEmail(): string {
		return $this->uniqueEmail() . '.' . $this->uniqueEmail() . '@' . $this->uniqueEmail() . '.' . $this->random([
				'com', 'nl', 'net', 'de', 'voice', 'event'
			]);
	}


	/**
	 * Returns a random Time
	 *
	 * @return FrozenTime
	 */
	public function randomTime(): FrozenTime {
		return FrozenTime::create(
			$this->random([2000, 2016, 2017, 2018]),
			$this->random([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
			$this->randomInt(31, 1),
			$this->randomInt(23),
			$this->randomInt(59)
		);
	}

	/**
	 * Creates a random DateTime object
	 *
	 * @return DateTime
	 */
	public function randomDateTime(): DateTime {
		return new DateTime();
	}

}
