<?php
/**
 * Created by PhpStorm.
 * User: lordphnx
 * Date: 15/08/2018
 * Time: 14:14
 */

namespace EZCake\EasyTest;


use Cake\Cache\Cache;
use Cake\Mailer\AbstractTransport;
use Cake\Mailer\Email;
use Cake\Mailer\Message;
use Cake\Mailer\Transport\SmtpTransport;
use Cake\View\ViewBuilder;
use Exception;

/**
 * An email transport for testing
 *
 * @internal every mail is put into a cache, adn then forwarded to an SMTP transport, if such a transport is configured
 * @package FenixDigitalSolutions\JoshUtils\EasyTest
 */
class TestTransport extends AbstractTransport {

	private static $key = "Fenix-Digital-Soltions.EasyTest.Mail";

	private $parent;


	public function __construct(array $config = []) {
		parent::__construct($config);
		if (!empty($config)) {
			try {
				$this->parent = new SmtpTransport($config);
			} catch (Exception $ex) {

			}
		}

	}

    /**
     * Send mail
     *
     * @param Message $message Email instance.
     * @return array
     */
	public function send(Message $message) : array {

		$mails = self::getAllMails();
		
		
		
		$mails[] = [
			"from" => $message->getFrom(),
			"to" => $message->getTo(),
			"subject" => $message->getSubject(),
			"body" => $message->getBodyHtml()
		];
		Cache::write(self::$key, $mails);

		try {
			$this->parent->send($message);
		} catch (Exception $ex) {

		}
		return $mails;
	}


	/**
	 * Returns an array of data (e.g. from, to, subject, body) of all mails sent since the last clear
	 * @return array
	 */
	public static function getAllMails(): array {
		$data = Cache::read(self::$key);
		if (empty($data)) {
			$data = [];
		}
		return $data;
	}

	/**
	 * Returns the number of mails sent by the Transport since the last clear
	 */
	public static function getMailCount() {
		return count(self::getAllMails());
	}

	/**
	 * Clears all mails in the transport
	 */
	public static function clearAllMails(): void {
		Cache::delete(self::$key);
	}

	public static function deleteMail($index): void {
		$mails = Cache::read(self::$key, []);
		unset($mails[$index]);
		Cache::write(self::$key, $mails);
	}


	public static function hasMailContaining($content) {
		/** @var Email $mail */
		foreach (self::getAllMails() as $mail) {

			if (strpos($mail['body'], $content) !== false) {
				return true;
			}

		}
		return false;
	}

	/**
	 * Checks if there is a mail with exactly the provided subject
	 * @param $subject
	 * @return bool
	 */
	public static function hasMailWithSubject($subject): bool {
		/** @var Email $mail */
		foreach (self::getAllMails() as $mail) {
			if ($mail['subject'] == $subject) {
				return true;
			}
		}
		return false;
	}


}