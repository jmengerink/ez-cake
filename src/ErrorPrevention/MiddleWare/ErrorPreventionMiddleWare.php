<?php
/**
 * Created by PhpStorm.
 * User: lordphnx
 * Date: 30/10/2018
 * Time: 11:35
 */

namespace EZCake\ErrorPrevention\MiddleWare;


use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\Exception\RedirectException;
use Cake\Http\Response;
use EZCake\ErrorPrevention\Preventers\PreventerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

class ErrorPreventionMiddleWare extends ErrorHandlerMiddleware {

	/**
	 * @var PreventerInterface[]
	 */
	private $preventers;

	public function __construct($preventers = [], $errorHandler = []) {

		parent::__construct([]);
		$this->preventers = $preventers;
	}


	/**
	 * Wrap the remaining middleware with error handling.
	 *
	 * @param ServerRequestInterface $request The request.
	 * @param RequestHandlerInterface $handler The request handler.
	 *
	 * @return ResponseInterface A response.
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		//pre: try to block weird stuff
		foreach ($this->preventers as $preventer) {
			if ($preventer->shouldBlock($request)) {
				return new Response([
					'status' => 412,
					'body' => 'Incursion attempt prevented'
				]);
			}
		}

		try {
			return $handler->handle($request);
		} catch (RedirectException $exception) {
			return $this->handleRedirect($exception);
		} catch (Throwable $exception) {
			return $this->handleException($exception, $request);
		}
	}


	public function handleException(Throwable $exception, ServerRequestInterface $request): ResponseInterface {

		foreach ($this->preventers as $preventer) {
			if ($preventer->shouldSkipReport($request, $exception)) {
				return $this->handleExceptionWithoutLog($exception, $request);
			}
		}

		//regular error handling, including logging
		return parent::handleException($exception, $request);
	}

	private function handleExceptionWithoutLog(Throwable $exception, ServerRequestInterface $request) {
		return $this->getErrorHandler()->getRenderer($exception, $request)->render();
	}


}