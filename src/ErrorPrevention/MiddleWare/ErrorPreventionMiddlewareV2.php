<?php
/**
 * Created by PhpStorm.
 * User: lordphnx
 * Date: 30/10/2018
 * Time: 11:35
 */

namespace EZCake\ErrorPrevention\MiddleWare;


use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\Exception\RedirectException;
use Cake\Http\Response;
use EZCake\ErrorPrevention\Preventers\PreventerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

/**
 * A re-implementation of {@link ErrorPreventionMiddleWare}, but this one doesn't do the error rendering for non-prevented errors
 * Should be "under" the regular {@link ErrorHandlerMiddleware}
 */
class ErrorPreventionMiddlewareV2 implements MiddlewareInterface {

	/**
	 * @var PreventerInterface[]
	 */
	private $preventers;

	public function __construct($preventers = []) {
		$this->preventers = $preventers;
	}


	/**
	 * Wrap the remaining middleware with error handling.
	 *
	 * @param ServerRequestInterface $request The request.
	 * @param RequestHandlerInterface $handler The request handler.
	 *
	 * @return ResponseInterface A response.
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		//pre: try to block weird stuff
		foreach ($this->preventers as $preventer) {
			if ($preventer->shouldBlock($request)) {
				return new Response([
					'status' => 412,
					'body' => 'Incursion attempt prevented'
				]);
			}
		}

		try {
			return $handler->handle($request);
		} catch (\Exception $exception) {
			foreach ($this->preventers as $preventer) {
				if ($preventer->shouldSkipReport($request, $exception)) {
					return new Response([
						'status' => 412,
						'body' => 'Incursion attempt detected'
					]);
				}
			}
			throw $exception;
		}
	}


}