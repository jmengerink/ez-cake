<?php


namespace EZCake\ErrorPrevention\Preventers;

/**
 * Blocks exceptions related to wordpress
 *
 * @package ErrorPrevention\Preventers
 */
class ForeignExtensionPreventer extends UrlRegexPreventer {


	public static $blockPath = [
		'/[.]asp$/i',
		'/[.]aspx$/i',
		'/[.]jsp$/i',
		'/[.]jspx$/i',
		'/[.]py/i',
		'/[.]php/i',
	];
	

}