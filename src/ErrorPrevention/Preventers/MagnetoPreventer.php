<?php


namespace  EZCake\ErrorPrevention\Preventers;


class MagnetoPreventer extends UrlRegexPreventer {

	public static $blockPath = [
		'/magneto/i'
	];
}