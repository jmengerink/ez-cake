<?php


namespace EZCake\ErrorPrevention\Preventers;


use Psr\Http\Message\ServerRequestInterface;
use Throwable;

/**
 * @todo @josh document
 * @package ErrorPrevention\Preventers
 */
interface PreventerInterface {


	public function shouldBlock(ServerRequestInterface $request): bool;

	public function shouldSkipReport(ServerRequestInterface $request, Throwable $throwable): bool;
}