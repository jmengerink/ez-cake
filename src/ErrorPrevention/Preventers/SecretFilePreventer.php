<?php


namespace EZCake\ErrorPrevention\Preventers;

/**
 * Blocks exceptions related to wordpress
 *
 * @package ErrorPrevention\Preventers
 */
class SecretFilePreventer extends UrlRegexPreventer {


	public static $blockPath = [
		'/[.]env$/i',
		'/[.]ads[.]txt/i',
		'/[.]well-known[.]txt/i',
		'/[.]humans[.]txt/i',
		'/[.]git/i' //.git
	];
	

}