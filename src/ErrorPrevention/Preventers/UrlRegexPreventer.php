<?php


namespace EZCake\ErrorPrevention\Preventers;


use Psr\Http\Message\ServerRequestInterface;
use Throwable;

abstract class UrlRegexPreventer implements PreventerInterface {
	protected static $blockUrl = [];
	protected static $blockPath = [];

	protected static $skipReportUrl = [];
	protected static $skipReportPath = [];

	public function shouldBlock(ServerRequestInterface $request): bool {
		foreach ($this::$blockPath as $pattern) {
			if (preg_match($pattern, $request->getUri()->getPath()) === 1) {
				return true;
			}
		}
		foreach ($this::$blockUrl as $pattern) {
			if (preg_match($pattern, "{$request->getUri()}") === 1) {
				return true;
			}
		}
		return false;
	}

	public function shouldSkipReport(ServerRequestInterface $request, Throwable $throwable): bool {
		foreach ($this::$skipReportPath as $pattern) {
			if (preg_match($pattern, $request->getUri()->getPath()) === 1) {
				return false;
			}
		}
		foreach ($this::$skipReportUrl as $pattern) {
			if (preg_match($pattern, "{$request->getUri()}") === 1) {
				return false;
			}
		}
		return true;
	}
}