<?php

namespace EZCake\Test\TestCase\EasyCache;

use Cake\TestSuite\TestCase;
use EZCake\EasyCache\LoadingSizeCache;

class LoadSizeCacheTest extends TestCase {


	public function testSimple() {
		$cache = new LoadingSizeCache(static function ($key) {
			return $key;
		},10);
		
		for ($i=0;$i<20;$i++) {
			self::assertEquals($i, $cache->get($i));
			self::assertTrue($cache->getSize() <= 10);
		}
	}

	public function testHandlesNull() {
		$this->loadCounter = 0;
		$cache = new LoadingSizeCache(function ($key) {
			$this->loadCounter++;
			return null;
		},10);

		for ($i=0;$i<10;$i++) {
			$cache->get($i);
			$cache->get($i);
		}
		
		self::assertEquals(10, $this->loadCounter);
	}
	
	public function testRemovesCorrectly() {
		
		$cache = new LoadingSizeCache(function ($key) {
			$this->fail('Test failed! Load called');
			throw new \Exception('Test failed!');
		},10);
		
		for ($i=0;$i<10;$i++) {
			$cache->cache($i,$i);
		}
		self::assertEquals(10, $cache->getSize());
				
		
		self::assertEquals(5, $cache->get(5));
		
		
	}
}