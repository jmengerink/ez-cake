<?php

namespace EZCake\Test\TestCase\EasyTest;

use Cake\TestSuite\TestCase;
use EZCake\EasyTest\EasyTestTrait;

class EasyTestTraitTest extends TestCase {
	use EasyTestTrait;

	public function testRandomInt(): void {
		$x = $this->randomInt();
		self::assertIsInt($x);
	}

	public function testRandomIntWithUb(): void {
		$x = $this->randomInt(100);
		self::assertIsInt($x);
	}

	public function testRandomIntWithBounds(): void {
		$x = $this->randomInt(100,0);
		self::assertIsInt($x);
	}

	public function testRandomEmail(): void {
		$x = $this->randomEmail();
		self::assertNotFalse(filter_var($x, FILTER_VALIDATE_EMAIL));
	}
	
	public function testRandom(): void {
		$x = $this->random(['a','b','c']);
		self::assertIsString($x);
	}

	public function testUniqueString(): void {
		$out = [];
		for ($i=0;$i<1000;$i++) {
			$out[] = $this->uniqueString();
		}
		$out = array_unique($out);
		self::assertCount(1000, $out);
	}

	/**
	 * @throws \JsonException
	 */
	public function testGenericRandomFloat(): void {
		$output = $this->genericRandom('float', '4,4');
		self::assertEquals((float)$output, $output);
	}

	/**
	 * @throws \JsonException
	 */
	public function testGenericRandomDoubleNoLength(): void {
		$output = $this->genericRandom('double', null);
		self::assertEquals((float)$output, $output);
	}

	/**
	 * @throws \JsonException
	 */
	public function testGenericRandomDouble(): void {
		$output = $this->genericRandom('double', '4,4');
		self::assertEquals((float)$output, $output);
	}

	/**
	 * @throws \JsonException
	 */
	public function testGenericRandomString(): void {
		self::assertIsString($this->genericRandom('string'));
	}

	public function testRandomBoolean(): void {
		$x1 = $this->randomBoolean();
		self::assertIsBool($x1);
		
		do {
			$x2 = $this->randomBoolean();
			self::assertIsBool($x2);
		} while ($x1 === $x2);
		self::assertNotEquals($x1, $x2);
		
	}
}